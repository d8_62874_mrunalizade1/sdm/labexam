const express = require('express')

const app = express()

app.use(express.json())

const bookrouter = require('./routes/books')

app.use('/book', bookrouter)

app.listen(5000, '0.0.0.0', () => {
  console.log(`server started on port 5000`)
})
